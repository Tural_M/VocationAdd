﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp9
{
    public partial class VocationAdd : Form
    {
        public VocationAdd()
        {
            InitializeComponent();
        }
        public static List<Workers> Workerlist;
        Workers workers = new Workers();
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Workers.Id++;
            workers.Name = textBox1.Text;
            workers.Surname = textBox2.Text;
            workers.DepartmentName = comboBox1.Text;
            workers.StartDate = dateTimePicker1.Value;
            Workerlist.Add(workers);
            Close();
        }

        private void VacationAdd_Load(object sender, EventArgs e)
        {
            Workerlist = new List<Workers>();
            foreach (var item in Department.ListDepart)
            {
                comboBox1.Items.Add(item.DepartName);
            }
        }
    }
    public class Workers
    {
        public static int Id ;
        public string Name { get; set; }
        public string Surname { get; set; }
        public string DepartmentName { get; set; }
        public DateTime StartDate { get; set; }
    }
}
