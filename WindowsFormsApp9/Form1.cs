﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            panel3.Visible = false;
        }
        Department department = null;
       
        VocationAdd vocationAdd = null;
        private void button1_MouseHover(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = Color.Blue;
            btn.ForeColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = SystemColors.Control;
            btn.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(vocationAdd ==  null)
            {
                vocationAdd = new VocationAdd();
                vocationAdd.FormClosed += VocationAdd_FormClosed;
                vocationAdd.Show();
            }
            else
            {
                vocationAdd.Activate();
            }
        }

        private void VocationAdd_FormClosed(object sender, FormClosedEventArgs e)
        {

            foreach (var item in VocationAdd.Workerlist)
            {
                ListViewItem lvitems = new ListViewItem(new string[] {Workers.Id.ToString(), item.Name, item.Surname, item.DepartmentName, item.StartDate.ToString() });
                listView1.Items.Add(lvitems);
            }
            vocationAdd = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(department == null)
            {
                department = new Department();
                department.FormClosed += Department_FormClosed;
                department.Show();
            }
            else
            {
                department.Activate();
            }
        }

        private void Department_FormClosed(object sender, FormClosedEventArgs e)
        {
            panel3.Visible = false;
            department = null;
        }
    }
}
